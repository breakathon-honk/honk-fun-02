# Deploy Notes

## Install flux

export GITLAB_TOKEN=<redacted>

flux bootstrap gitlab \
    --ssh-hostname=gitlab.com \
    --owner=breakathon-honk \
    --repository=honk-fun-02 \
    --branch=main \
    --path=clusters/kbreak-fun-honk
